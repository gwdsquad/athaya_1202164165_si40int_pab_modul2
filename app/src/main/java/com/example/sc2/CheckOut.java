package com.example.sc2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class CheckOut extends AppCompatActivity {

    private TextView tujuan;
    private TextView tglBerangkat;
    private TextView tglPulang;
    private TextView wktBerangkat;
    private TextView wktPulang;
    private TextView tiket;
    private TextView harga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        tujuan = findViewById(R.id.tujuan);
        tglBerangkat = findViewById(R.id.tglBerangkat);
        wktBerangkat = findViewById(R.id.waktuBerangkat);
        tiket = findViewById(R.id.jmlTiket);
        harga = findViewById(R.id.hargaTotal);

        Intent intent = getIntent();
        String msgTgl = intent.getStringExtra(home.EXTRA_MESSAGE);
        tglBerangkat.setText(msgTgl);
        String msgWkt = intent.getStringExtra(home.EXTRA_WKT);
        wktBerangkat.setText(msgWkt);
        String msgTujuan = intent.getStringExtra(home.EXTRA_TUJUAN);
        tujuan.setText(msgTujuan);
        int msgTiket = intent.getIntExtra(home.EXTRA_TIKET,0);
        tiket.setText("" + msgTiket);
        int msgHarga = intent.getIntExtra(home.EXTRA_HARGA,0);
        harga.setText("Rp"+msgHarga);
        String msgTglPulang = intent.getStringExtra(home.EXTRA_TGLPULANG);
        tglPulang.setText(msgTglPulang);


    }
}
