package com.example.sc2;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class home extends AppCompatActivity implements AdapterView.OnItemSelectedListener, NumberDialog.NumberDialogListener, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    private Button ButtonTopUp;
    private TextView saldo;

    private TextView pilihWaktu;
    public TextView pilihTanggal;

    private Switch pulangPergi;

    private TextView pilihWaktuPulang;
    private TextView pilihTanggalPulang;

    private Button button;

    public int jmlSaldo;

    public String destinasi;
    public int hargaSatuan;

    private EditText jmlTiket;

    private Spinner spinner;

    public int jumlahTiket;

    public int hargaTotal;

    public static final String EXTRA_MESSAGE = "com.example.android.sc2.extra.MESSAGE";
    public static final String EXTRA_WKT = "com.example.android.sc2.extra.WKT";
    public static final String EXTRA_TUJUAN = "com.example.android.sc2.extra.TUJUAN";
    public static final String EXTRA_TIKET = "com.example.android.sc2.extra.TIKET";
    public static final String EXTRA_HARGA = "com.example.android.sc2.extra.HARGA";
    public static final String EXTRA_TGLPULANG = "com.example.android.sc2.extra.TGLPULANG";
    public static final String EXTRA_WKTPULANG = "com.example.android.sc2.extra.WKTPULANG";








    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        spinner = findViewById(R.id.listTujuan);
        List<Tujuan> listTujuan = new ArrayList<>();
        Tujuan tuj1 = new Tujuan("Jakarta", 85000);
        Tujuan tuj2 = new Tujuan("Cirebon", 150000);
        Tujuan tuj3 = new Tujuan("Bekasi", 75000);
        listTujuan.add(tuj1);
        listTujuan.add(tuj2);
        listTujuan.add(tuj3);

        ArrayAdapter<Tujuan> adapter = new ArrayAdapter<Tujuan>(this,
                android.R.layout.simple_spinner_item, listTujuan);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Tujuan tujuan =(Tujuan) parent.getSelectedItem();
                dataTujuan(tujuan);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });







        ButtonTopUp = findViewById(R.id.topUp);
        saldo = findViewById(R.id.saldo);
        ButtonTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNumberDialog();
            }
        });

        pilihWaktu = findViewById(R.id.waktu);
        pilihWaktu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker = new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(), "time picker");
            }
        });

        pilihTanggal = findViewById(R.id.tanggal);
        pilihTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");

            }
        });

        pilihWaktuPulang = findViewById(R.id.waktuPulang);
        pilihWaktuPulang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment pulangTimePicker = new TimePickerFragment();
                pulangTimePicker.show(getSupportFragmentManager(), "pulang time picker");
            }
        });

        pilihTanggalPulang = findViewById(R.id.tanggalPulang);
        pilihTanggalPulang.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment pulangDatePicker = new DatePickerFragment();
                pulangDatePicker.show(getSupportFragmentManager(), "pulang date picker");
            }
        }));




        pulangPergi = findViewById(R.id.pulangPergi);
        pulangPergi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pulangPergi.isChecked())  {
                    pilihWaktuPulang.setVisibility(View.VISIBLE);
                    pilihTanggalPulang.setVisibility(View.VISIBLE);

                } else {
                    pilihWaktuPulang.setVisibility(View.INVISIBLE);
                    pilihTanggalPulang.setVisibility(View.INVISIBLE);

                }

            }
        });

        button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                jmlTiket = findViewById(R.id.jmlTiket);
                jumlahTiket = Integer.parseInt(jmlTiket.getText().toString());



                jmlSaldo = Integer.parseInt(saldo.getText().toString());

                if (pulangPergi.isChecked()){
                    hargaTotal = 2 * jumlahTiket * hargaSatuan;
                } else {
                    hargaTotal = jumlahTiket * hargaSatuan;
                }



                //hargaTotal = jumlahTiket * hargaSatuan;


                if (jmlSaldo < hargaTotal){
                    String txt = "Maaf saldo kamu kurang. Top up saldo dulu!";
                    Toast toast = Toast.makeText(getApplicationContext(),txt,Toast.LENGTH_SHORT);
                    toast.show();

                } else {
                    openCheckOutAct();


                }
            }
        });


    }

    //public void getSelectedUser (View v){
      //  Tujuan tujuan = (Tujuan) spinner.getSelectedItem();
    //}

    //private void displayTujuanData (Tujuan tujuan){

    //}


    public void openCheckOutAct(){
        Intent intent = new Intent(this, CheckOut.class);
        String msgTanggal = pilihTanggal.getText().toString();
        String msgWaktu = pilihWaktu.getText().toString();
        String msgTglPulang = pilihTanggalPulang.getText().toString();



        intent.putExtra(EXTRA_MESSAGE, msgTanggal);
        intent.putExtra(EXTRA_WKT,msgWaktu);
        intent.putExtra(EXTRA_TUJUAN,destinasi);
        intent.putExtra(EXTRA_TIKET, jumlahTiket);
        intent.putExtra(EXTRA_HARGA,hargaTotal);

        if (pulangPergi.isChecked()==true){
            intent.putExtra(EXTRA_TGLPULANG, msgTglPulang);
        }





        startActivity(intent);

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String currentDateString = DateFormat.getDateInstance().format(c.getTime());
        //pilihTanggal.setText(currentDateString);
        if (pulangPergi.isChecked()==false){
            pilihTanggal.setText(currentDateString);
        } else {
            pilihTanggalPulang.setText(currentDateString);
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        //pilihWaktu.setText(hourOfDay + ":" + minute);
        if (pulangPergi.isChecked()==false){
            pilihWaktu.setText(hourOfDay + ":" + minute);
        } else {
            pilihWaktuPulang.setText(hourOfDay + ":" + minute);
        }

    }

    private void openNumberDialog() {
        NumberDialog numberDialog = new NumberDialog();
        numberDialog.show(getSupportFragmentManager(), "enter number");
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(), text, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void applyText(String number) {
        saldo.setText(number);



    }
    public void getSelectedTujuan (View v){
        Tujuan tujuan = (Tujuan) spinner.getSelectedItem();
        dataTujuan(tujuan);

    }

    public void dataTujuan (Tujuan tujuan){
        destinasi = tujuan.getTujuan();
        hargaSatuan = tujuan.getHarga();

    }

}
