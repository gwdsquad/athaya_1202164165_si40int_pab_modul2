package com.example.sc2;

public class Tujuan {
    private String tujuan;
    private int harga;

    public Tujuan(String tujuan, int harga) {
        this.tujuan = tujuan;
        this.harga = harga;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    @Override
    public String toString(){
        String stringTujuan = tujuan + "(" + harga + ")";
        return stringTujuan;
    }
}
